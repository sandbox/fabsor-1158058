<?php

/**
 * @file
 * Installation schema for entityblock.
 */

/**
 * Implements hook_schema().
 */
function entityblock_schema() {
  $schema['entityblock_type'] = array(
    'description' => 'Stores entity block types',
    'fields' => array(
      'type_id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique entity_test type ID.',
      ),
      'type' => array(
        'description' => 'The machine-readable name of the entityblock type.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'The human-readable name of this entityblock type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ) + entity_exportable_schema_fields(),
    'primary key' => array('type_id'),
    'unique keys' => array(
      'type' => array('type'),
    ),
  );
  $schema['entityblock'] = array(
    'description' => 'Stores Blocks as entities.',
    'fields' => array(
      'bid' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'A serial ID for the entity block. It is here because entity api demands it.',
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'description' => 'Machine name of this block.',
      ),
      'title' => array(
        'description' => 'The block title.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'type' => array(
        'description' => 'The block type.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'cache' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
        'size' => 'tiny',
        'description' => 'Binary flag to indicate block cache mode. (-2: Custom cache, -1: Do not cache, 1: Cache per role, 2: Cache per user, 4: Cache per page, 8: Block cache global) See DRUPAL_CACHE_* constants in ../includes/common.inc for more detailed information.',
      ),
    ) + entity_exportable_schema_fields(),
    'primary key' => array('bid'),
    'unique keys' => array(
      'name' => array('name'),
    ),
    'foreigh keys' => array(
      'type' => array(
        'table' => 'entityblock_type',
        'columns' => array('type' => 'type'),
      ),
    ),
  );
  return $schema;
}