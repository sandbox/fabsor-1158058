<?php
/**
 * Generates the entity block type editing form.
 */
function entityblock_type_form($form, &$form_state, $block_type, $op = 'edit') {
  if ($op == 'clone') {
    $block_type->label .= ' (cloned)';
    $block_type->type .= '_clone';
  }
  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($block_type->label) ? $block_type->label : '',
  );
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($block_type->type) ? $block_type->type : '',
    '#machine_name' => array(
      'exists' => 'entityblock_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this block type. It must only contain lowercase letters, numbers, and underscores.'),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save block type'),
    '#weight' => 40,
  );
  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function entityblock_type_form_submit(&$form, &$form_state) {
  $block_type = entity_ui_form_submit_build_entity($form, $form_state);
  // We need to rebuild the menu in order for the entry to show up.
  menu_rebuild();
  $block_type->save();
  $form_state['redirect'] = 'admin/structure/entityblocks';
}

function entityblock_add_form($form, &$form_state, $type) {
  $form = array();
  $form_state['type'] = $type;
  $form = entityblock_form($form, $form_state);
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save block'),
    '#weight' => 40,
  );
  return $form;
}

function entityblock_edit_form($form, &$form_state, $entityblock) {
  $form = array();
  $form_state['entityblock'] = $entityblock;
  $form = entityblock_form($form, $form_state);
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save block'),
    '#weight' => 40,
  );
  return $form;  
}

function entityblock_delete_form($form, &$form_state, $entityblock) {
  $form_state['entityblock'] = $entityblock;
  return confirm_form($form, t('Are you sure you want to delete this block?'), 'admin/structure/entityblock');
}

function entityblock_delete_form_submit($form, &$form_state) {
  entity_delete('entityblock', $form_state['entityblock']->bid);
  $form_state['redirect'] = 'admin/structure/entityblock';
}

/**
 * Form for creating entity blocks.
 * @param array $form
 * @param array $form_state
 *   The form state should contain at least the type of the entity block.
 * @return array the form for the entity block.
 */
function entityblock_form($form = array(), &$form_state = array()) {
  // If the user has provided a block for us, then we should probably use
  // that. Otherwise, create a blank entity block.
  if (!isset($form_state['entityblock'])) {
    $form_state['entityblock'] = new EntityBlock;
    $form_state['entityblock']->type = $form_state['type'];
  }
  $entityblock = $form_state['entityblock'];
  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => $entityblock->title,
    '#title' => 'title',
  );
  // Machine-readable name.
  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => $entityblock->name,
    '#disabled' => isset($entityblock->name),
    '#required' => TRUE,
    '#machine_name' => array(
      'exists' => 'entityblock_load',
      'source' => array('title'),
    ),
    '#description' => t('A unique machine-readable name for this block. It must only contain lowercase letters, numbers, and underscores.'),
  );
  // We let the user set whatever kind of caching should be used for this block.
  $form['cache'] = array(
    '#type' => 'select',
    '#title' => t('Cache Setting'),
    '#default_value' => $entityblock->cache,
    '#options' => array(
      DRUPAL_NO_CACHE => t('No caching'),
      DRUPAL_CACHE_GLOBAL => t('Cache once for all users'),
      DRUPAL_CACHE_PER_PAGE => t('Cache per page'),
      DRUPAL_CACHE_PER_ROLE => t('Cache per role'),
      DRUPAL_CACHE_PER_USER => t('Cache per user'),
    ),
  );
  field_attach_form('entityblock', $entityblock, $form, $form_state);
  $form['#validate'][] = 'entityblock_form_validate';
  $form['#submit'][] = 'entityblock_form_submit';
  return $form;
}

/**
 * Validate our form.
 * @param array $form
 * @param array $form_state
 */
function entityblock_form_validate($form, &$form_state) {
  // Fetch the block and populate it. Since this is very simple to do,
  // let's just do it ourselves instead of using the entity api function.
  $entityblock = &$form_state['entityblock'];
  $entityblock->name = $form_state['values']['name'];
  $entityblock->title = $form_state['values']['title'];
  $entityblock->cache = $form_state['values']['cache'];
  field_attach_form_validate('entityblock', $entityblock, $form, $form_state);
}

/**
 * Finally time to actually save the block.
 * @param type $form
 * @param array $form_state
 */
function entityblock_form_submit($form, &$form_state) {
  $entityblock = $form_state['entityblock'];
  field_attach_submit('entityblock', $entityblock, $form, $form_state);
  entity_save('entityblock', $entityblock);
  $form_state['redirect'] = 'admin/structure/block';
}

/**
 * An admin overview page.
 */
function entityblock_admin() {
  // Get all entities.
  $blocks = entityblock_load_multiple();
  $block_table = array();
  $block = new EntityBlock;
  foreach ($blocks as $block) {
    $operations = l(t('Edit'), 'admin/structure/entityblock/edit/' . $block->name);
    if (!$block->hasStatus(ENTITY_IN_CODE)) {
       $operations .= ' | ' . l(t('Delete'), 'admin/structure/entityblock/delete/' . $block->name);
    }
    $block_table[] = array(
      $block->title, 
      $block->type,
      $operations,
    );
  }
  return theme('table', array('rows' => $block_table, 'header' => array('Title', 'Type', 'Operations')));
}