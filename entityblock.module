<?php
/**
 * @file
 * Tha main module file for entity block. Mostly hook implementations here.
 */

/**
 * Implements hook_entity_info().
 * Defines the basic block entity.
 */
function entityblock_entity_info() {
  return array(
    'entityblock_type' => array(
      'label' => t('Block type'),
      'entity class' => 'Entity',
      'controller class' => 'EntityAPIController',
      'base table' => 'entityblock_type',
      'fieldable' => FALSE,
      'bundle of' => 'entityblock',
      'exportable' => TRUE,
      'entity keys' => array(
        'id' => 'type_id',
        'name' => 'type',
        'label' => 'label',
      ),
      'access callback' => 'entityblock_type_access',
      'module' => 'entityblock',
      // Enable the entity API's admin UI.
      'admin ui' => array(
        'path' => 'admin/structure/entityblocks',
      ),
    ),
    'entityblock' => array(
      'label' => t('Entity Block'),
      'entity class' => 'EntityBlock',
      'controller class' => 'EntityAPIController',
      'fieldable' => TRUE,
      'exportable' => TRUE,
      'view modes' => array(
        'block' => array(
          'label' => t('Block'),
          'custom settings' => FALSE,
        ),
      ),
      'access callback' => 'entityblock_access',
      'base table' => 'entityblock',
      'module' => 'entityblock',
      'bundles' => array(),
      'bundle keys' => array('bundle' => 'type'),
      'entity keys' => array(
        // We need to have a serial ID for all entities. Damn it!
        'id' => 'bid',
        'name' => 'name',
        'bundle' => 'type',
        'label' => 'title',
      ),
    ),
  );
}

/**
 * Implements hook_entity_info_alter().
 *
 * Use this hook to specify profile bundles to avoid a recursion, as loading
 * the entity block types needs the entity info too.
 */
function entityblock_entity_info_alter(&$entity_info) {
  foreach (entityblock_get_types() as $type => $info) {
    $entity_info['entityblock']['bundles'][$type] = array(
      'label' => $info->label,
      'admin' => array(
        'path' => 'admin/structure/entityblocks/manage/%entityblock_type',
        'real path' => 'admin/structure/entityblocks/manage/' . $type,
        'bundle argument' => 4,
        'access arguments' => array('administer entity block types'),
      ),
    );
  }
}

/**
 * Implements hook_menu().
 */
function entityblock_menu() {
  $items = array();
  $block_types = entityblock_get_types();
  // If we are using the block module, then we add links from there.
  foreach ($block_types as $key => $info) {
    $item = array(
      'title' => 'Add ' . drupal_strtolower($info->label),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('entityblock_add_form', 4),
      'access arguments' => array("create {$info->type} entity blocks"),
      'file' => 'entityblock.admin.inc',
      'type' => MENU_LOCAL_ACTION,
    );
    if (module_exists('block')) {
      $items['admin/structure/block/entityblock-add/' . $info->type] = $item;
    }
    $items['admin/structure/entityblock/add/' . $info->type] = $item;
  }
  // If not, then we need to provide our own UI.
  $items['admin/structure/entityblock'] = array(
    'title' => 'Entity Blocks',
    'page callback' => 'entityblock_admin',
    'description' => t('Administer entity blocks'),
    'access arguments' => array('administer entity blocks'),
    'file' => 'entityblock.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/structure/entityblock/edit/%entityblock'] = array(
    'title' => 'Edit entity',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('entityblock_edit_form', 4),
    'access arguments' => array('edit', 4),
    'access callback' => 'entityblock_access',
    'file' => 'entityblock.admin.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/structure/entityblock/delete/%entityblock'] = array(
    'title' => 'Delete entity',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('entityblock_delete_form', 4),
    'access callback' => 'entityblock_access',
    'access arguments' => array('delete', 4),
    'file' => 'entityblock.admin.inc',
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function entityblock_permission() {
  $permissions = array(
    'administer entity block types' =>  array(
      'title' => t('Administer entity block types'),
      'description' => t('Create and delete fields on user entity blocks, and set their permissions.'),
    ),
  );
  // Generate per profile type permissions.
  foreach (entityblock_get_types() as $type) {
    $type_name = check_plain($type->type);
    $permissions += array(
      "create $type_name entity blocks" => array(
        'title' => t('%type_name: Create entity block', array('%type_name' => $type->label)),  
      ),
      "edit $type_name entity blocks" => array(
        'title' => t('%type_name: Edit entity block', array('%type_name' => $type->label)),
      ),
      "view $type_name entity blocks" => array(
        'title' => t('%type_name: View entity block', array('%type_name' => $type->label)),
      ),
      "delete $type_name entity blocks" => array(
        'title' => t('%type_name: Delete entity block', array('%type_name' => $type->label)),
      ),
    );
  }
  return $permissions;
}

/**
 * Implements hook_block_info().
 */
function entityblock_block_info() {
  $blocks = array();
  // Load all blocks
  $entityblocks = entityblock_load_multiple();
  foreach ($entityblocks as $entityblock) {
    $blocks[$entityblock->name] = array(
      'info' => $entityblock->title,
      'cache' => $entityblock->cache,
    );
  }
  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function entityblock_block_configure($delta) {
  $block = entityblock_load($delta);
  if (user_access("edit {$block->type} entity blocks")) {
    $form_state = array('type' => $block->type, 'entityblock' => $block);
    // We attach our entity block form here.
    return entityblock_form(array(), $form_state);
  }
}

/**
 * Implements hook_block_view().
 */
function entityblock_block_view($delta) {
  $block = array();
  // The delta is always the same as the machine name of the
  // entity block, so it's fairly easy to load.
  $entityblock = entityblock_load($delta);
  if ($entityblock && user_access("view {$entityblock->type} entity blocks")) {
    $block['subject'] = check_plain($entityblock->title);
    $block['content'] = $entityblock->buildContent();
    return $block;
  }
}

/**
 * Implements hook_form_block_admin_configure_alter().
 */
function entityblock_form_block_admin_configure_alter(&$form, &$form_state) {
  // We need to add our submit handlers here, the hook_block_configure hook
  // does not cut it for us since we need more detalied information about the form
  // in order for this to work with field_attach_form and friends.
  $block = entityblock_load($form['delta']['#value']);
  if (user_access("edit {$block->type} entity blocks")) {
    if (isset($block)) {
      $form_state['entityblock'] = $block;
      $form['#validate'][] = 'entityblock_form_validate';
      $form['#submit'][] = 'entityblock_form_submit';
    }
  }
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function entityblock_ctools_plugin_directory($module, $plugin) {
  if ($module == 'ctools' && !empty($plugin)) {
    return "plugins/$plugin";
  }
}

/**
 * Load an entity block.
 * @param string name
 *  the name of the block.
 * @return EntityBlock
 *  The block if one was found, FALSE otherwise.
 */
function entityblock_load($name) {
  $block = entity_load('entityblock', array($name));
  return isset($block[$name]) ? $block[$name] : FALSE;
}

/**
 * Load multiple blocks.
 * @param array $names
 *  The machine names of the blocks you want to load.
 * @return EntityBlock[]
 *  The blocks you specified, if any of them were found.
 */
function entityblock_load_multiple($names = FALSE) {
  return entity_load('entityblock', is_array($names) ? $names : FALSE);
}

/**
 * Load a particular entity block.
 * @param string $name
 * @return type
 */
function entityblock_type_load($name) {
  return entityblock_get_types($name);
}

/**
 * Gets an array of all entityblock types, keyed by the type name.
 *
 * @param $type_name
 *   If set, the type with the given name is returned.
 * @return EntityType[]
 *   Depending whether $type isset, an array of block types or a single one.
 */
function entityblock_get_types($type_name = NULL) {
  $types = entity_load('entityblock_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Access callback for the entity block types.
 */
function entityblock_type_access($op, $type = NULL, $account = NULL) {
  return user_access('administer entity block types', $account);
}

/**
 * Access callback for the entity block types.
 */
function entityblock_access($op, $block, $account = NULL) {
  if (isset($block) && $type_name = $block->type) {
    return user_access("$op $type_name profile", $account);
  }
}



/**
 * This class is used to represent Entity Blocks.
 */
class EntityBlock extends Entity {
  // We provide the values here for reference.
  public $type;
  public $name;
  public $bid;
  public $title;
  public $cache;

  public function __construct($values = array()) {
    parent::__construct($values, 'entityblock');
  }

  /**
   * Gets the type entity.
   *
   * @return Entity
   *  The type associated with this entity.
   */
  public function getTypeEntity() {
    if (isset($this->type)) {
      return entity_get_types($this->type);
    }
  }

  /**
   * Build content.
   * @param string $view_mode
   *   The current view mode.
   * @param type $langcode
   *   The language code to use.
   * @return array
   *  An array that can be used with Drupals rendering system. 
   */
  public function buildContent($view_mode = 'block', $langcode = NULL) {
    $content = array();
    return entity_get_controller($this->entityType)->buildContent($this, $view_mode, $langcode, $content);
  }
}
