<?php

/**
 * @file
 * Plugin to handle the 'node' content type which allows individual nodes
 * to be embedded into a panel.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Entity Block'),
);

function entityblock_entityblock_content_type_content_types() {
  $types = array();
  $defaults = array(
    'description' => t('An entity block.'),
    'category' => array(t('Entity Blocks'), -9),
    'defaults' => array(
      'name' => '',
      'type' => '',
      'title' => '',
    ),
  );
  // Get all entities.
  $entities = entityblock_load_multiple();
  foreach ($entities as $entity) {
    $type = $defaults;
    $type['title'] = t('%block entity block', array('%block' => $entity->title));
    $type['defaults']['name'] = $entity->name;
    $type['defaults']['type'] = $entity->type;
    $types[$entity->name] = $type;
  }
  return $types;
}

/**
 * Output function for the 'node' content type.
 *
 * Outputs a node based on the module and delta supplied in the configuration.
 */
function entityblock_entityblock_content_type_render($subtype, $conf, $panel_args) {
  $content = new stdClass;
  $entity = entityblock_load($conf['name']);
  $content->subject = $entity->title;
  $content->content = $entity->view();
  return $content;
}

/**
 * The form to add or edit a node as content.
 */
function entityblock_entityblock_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  
  return $form;
}

/**
 * Validate the node selection.
 */
function entityblock_entityblock_content_type_edit_form_validate(&$form, &$form_state) {

}

/**
 * Validate the node selection.
 */
function entityblock_entityblock_content_type_edit_form_submit($form, &$form_state) {
  //foreach (array('nid', 'links', 'leave_node_title', 'link_node_title', 'identifier', 'build_mode') as $key) {
  //  $form_state['conf'][$key] = $form_state['values'][$key];
  //}
}

/**
 * Returns the administrative title for a node.
 */
function entityblock_entityblock_content_type_admin_title($subtype, $conf) {
  return t('Entity Block: @var', array('@var' => $conf['name']));
}
